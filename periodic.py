import yaml
import datetime
from flask import Flask
from flask import render_template
from flask import redirect
from flask import request
from flask_bootstrap import Bootstrap
from flask_datepicker import datepicker
from tinydb import TinyDB, Query

app = Flask(__name__)
Bootstrap(app)
datepicker(app)


db = TinyDB("done.json")
date_format_string = "%y/%m/%d %H:%M"


def read_config():
    with open("tasks.yml", "r") as stream:
        return yaml.safe_load(stream)


def find_overdue_status(last_done_time, days, task_due_time):
    time_until_due = task_due_time - datetime.datetime.now()
    seconds_until_due = (time_until_due.days * 24 * 60 * 60) + time_until_due.seconds
    due_days_in_seconds = days * 24 * 60 * 60
    due_ratio = (due_days_in_seconds - seconds_until_due) / due_days_in_seconds
    # print(time_until_due,seconds_until_due,due_days_in_seconds,due_ratio)
    if due_ratio > 1:
        # Overdue
        return "bg-danger"
    elif due_ratio > 0.8:
        # Almost Due
        return "bg-warning"
    else:
        # OK
        return "bg-success"


def default_overdue_time(days):
    overdue_time = datetime.datetime.now() - datetime.timedelta(days=days)
    return overdue_time.strftime(date_format_string)


def task_due_date(task_name, days):
    TaskQuery = Query()
    last_done_query = db.search(TaskQuery.task == task_name)
    last_done_time_str = default_overdue_time(days)
    for last_done_task in last_done_query:
        last_done_time_str = last_done_task["time"]
    last_done_time = datetime.datetime.strptime(last_done_time_str, date_format_string)
    task_due_time = last_done_time + datetime.timedelta(days=days)
    # print(last_done_time,days,task_due_time)
    overdue_status = find_overdue_status(last_done_time, days, task_due_time)
    # print(overdue_status)
    return last_done_time, task_due_time, overdue_status


def set_task_done_time(task_name, done_time):
    TaskQuery = Query()
    task_time = done_time.strftime(date_format_string)
    db.remove(TaskQuery.task == task_name)
    db.insert({"task": task_name, "time": task_time})
    print(f"{task_name} set to {task_time}")


@app.route("/task/newdate", methods=["POST"])
def set_date_from_app():
    print(request.values)
    task_name = request.values["task_name"]
    new_date_str = request.values["new_date"]
    new_date = datetime.datetime.strptime(new_date_str, "%Y-%m-%d")
    set_task_done_time(task_name, new_date)
    return redirect("/")


@app.route("/task/set_date")
def task_set_date():
    tasks = read_config()["tasks"]
    return render_template("task_date.html", tasks=tasks)


@app.route("/do_task/<task_name>")
def do_task(task_name):
    set_task_done_time(task_name, datetime.datetime.now())
    return redirect("/tasks")


@app.route("/tasks")
def list_tasks():
    tasks = read_config()["tasks"]
    task_names = []
    for task in read_config():
        task_names.append(task)
    for task in tasks:
        last_done_time, task_due_time, overdue_status = task_due_date(
            task["name"], task["days"]
        )
        task["last_done"] = last_done_time
        task["overdue_status"] = overdue_status
        task["due_time"] = task_due_time
    return render_template("tasks.html", tasks=tasks)


@app.route("/")
def hello_world():
    return redirect("/tasks")


if __name__ == "__main__":
    read_config()
